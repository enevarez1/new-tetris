import controller.Controller;
import model.GameState;
import model.Tetromino;
import model.TetrominoMaker;
import view.TetrisUI;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

/**
 * Main method to run program
 * @author Team #6
 * Erick Nevarez
 * Alfred Laredo
 * Victor Fernandez
 *
 */

public class Tetris {

    //Test for git init


    public static void main(String[] args)
    {
        GameState gameState = new GameState();
        Controller controller = new Controller();
        TetrisUI gui = new TetrisUI(args);
        controller.setGame(gameState);
        controller.setGui(gui);
        gui.setActionListener(controller);
        gui.setKeyListener(controller);
        gui.setGame(gameState);
        gui.getNetworkUI().addWindowListener(controller);
        gui.getNetworkUI().run();
        gui.getNetworkUI().setActionListener(controller);
        gameState.addObserver(gui);
        gui.run();
        gui.start();


    }


}