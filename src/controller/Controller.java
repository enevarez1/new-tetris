/**
 *
 */
package controller;

import model.GameState;
import model.NetworkAdapter;
import model.NetworkMessageListener;
import model.Tetromino;
import view.TetrisUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

/**
 * Manages all events between View (GUI) and Model (Game State)
 * @author epadilla2
 *
 */
public class Controller implements KeyListener, NetworkMessageListener, ActionListener, WindowListener {

    private GameState gameState;
    private TetrisUI gui;
    public Thread testThread;

    /**
     * Sets gui to gui parameter
     *
     * @param gui TetrisUI Object
     */
    public void setGui(TetrisUI gui) {
        this.gui = gui;
    }

    /**
     * Sets gameState to gameState parameter
     *
     * @param gameState GameState Object
     */
    public void setGame(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Defines action when a key is pressed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();

        //pause
        if (keyCode == KeyEvent.VK_ESCAPE) {
            //your code goes here
            //Pause game code needed in GameState
            return;
        }
        boolean rep = true;
        switch (keyCode) {
            case KeyEvent.VK_DOWN:
                this.gameState.moveTetrominoDown();
                break;
            case KeyEvent.VK_LEFT:
                this.gameState.moveTetrominoLeft();
                break;
            case KeyEvent.VK_RIGHT:
                this.gameState.moveTetrominoRight();
                break;
            case KeyEvent.VK_Z:
                //add rotate in gameState
                this.gameState.rotateTetroLeft();
                break;
            case KeyEvent.VK_C:
                this.gameState.rotateTetroRight();
                break;
            default:
                rep = false;
                break;
        }
        if (rep) this.gui.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Starts multiplayer game
     */
    public void startMultiplayer()
    {
            this.gui.getNetworkUI().setVisible(false);
            this.gameState.newMultiGame();
            this.gui.resetTimer();
            this.gui.start();
    }

    /**
     * New Game Prompt
     * @param mpPlayerName Player Name for socket
     * @return true or false if they accept
     */
    public boolean newGameMessage(String mpPlayerName) {
        Object[] options = { "Yes", "No" };

        String acceptNewGame = (String)JOptionPane.showInputDialog(null, "User " + mpPlayerName + " wants to start a new game, do you want to play?\n",
                "New Game", 1, null, options, "Yes");

        return "Yes".equals(acceptNewGame);
    }

    @Override
    /**
     * Message Received provided by Professor
     */
    public void messageReceived(NetworkAdapter.MessageType type, String s, int x, int y, int z, int[] others) {
        Tetromino.TetrominoEnum[][] mpBoard;
        boolean newGame;
        NetworkAdapter networkAdapter = this.gameState.getNetworkAdapter();
        /*case NEW: ...      // s (player name)
         *          case NEW_ACK: ...  // s (other player name) x (response)
         *          case FILL: ...     // x (number of new lines)
         *			case STATUS: ...   // x (score), y (indicator if game is over yet), other (array containing board information)
         *          case QUIT: ... */
        switch (type) {
            case NEW:
                this.gameState.setMpName(s);
                newGame = newGameMessage(s);

                System.out.println("String "+this.gameState.getUserName()+" newGame" +newGame);
                networkAdapter.writeNewAck(this.gameState.getUserName(), newGame ? 1 : 0); //Accept 1, No 0
                if (newGame) {
                    startMultiplayer();

                    break;
                }
                networkAdapter.close();
                this.gui.getNetworkUI().addToTextArea("You do not want to play multiplayer");
                this.gui.getNetworkUI().setVisible(false);
                break;
            case NEW_ACK:
                if (x == 1) //User accepted
                {
                    this.gameState.setMpName(s);
                    startMultiplayer();
                }
                else
                    this.gui.getNetworkUI().addToTextArea("The other user does not find you worthy to play");
                break;
            case FILL:
                break;
            case STATUS:
                //Board Sending
                System.out.println("Status was called");
                this.gameState.setMpScore(x);
                this.gameState.setMpGameOver((y == 1));
                this.gui.repaint();
                break;
            case QUIT:
                if (networkAdapter != null)
                {
                    networkAdapter.close();
                }
                break;

        }

    }
    //Host and CLicked were modified from a youtube tutorial we used.

    /**
     * Host Clicked, used a youtube tutorial
     */
    public void hostClicked() {
        try {
            if (this.testThread != null && ( this.testThread == null ||this.testThread.isAlive())){
                if (this.testThread.isAlive())
                {

                    this.gameState.getNetworkAdapter().close();
                    this.testThread = null; }
                }
            else {
                URL url = new URL("http://checkip.amazonaws.com"); //Checking IP for local

                InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                inputStreamReader.close();
                bufferedReader.close();

                System.out.println(this.gui.getNetworkUI().portText.getText());
                ServerSocket serverSocket = new ServerSocket(Integer.parseInt(this.gui.getNetworkUI().portText.getText()));
                System.out.println("Socket Created");
                this.gui.getNetworkUI().addToTextArea("Socket has been created \n");

                this.testThread = new Thread //Lambda used from youtube just modified and taught in class
                        (new Runnable() {
                    public void run() {
                        try {
                            Socket socket = serverSocket.accept();
                            NetworkAdapter networkAdapter = new NetworkAdapter(socket);
                            networkAdapter.setServerSocket(serverSocket);

                            Controller.this.gameState.setNetworkAdapter(networkAdapter);
                            Controller mpController = Controller.this;

                            networkAdapter.setMessageListener(mpController::messageReceived);
                            networkAdapter.receiveMessagesAsync();
                        } catch (IOException networkException) {
                        }

                    }
                }
                );
                this.testThread.start();
            }
        } catch (IOException ioException) {
        }
    }

    /**
     * Connect clicked, used a youtube tutorial
     */
    public void connectClicked()
    {
        try {
            Controller controller = this;

            try {
                String host = this.gui.getNetworkUI().hostText.getText();
                int port = Integer.parseInt(this.gui.getNetworkUI().portText.getText());

                this.gui.getNetworkUI().addToTextArea("Attempting to Connect\n");
                Socket socket = new Socket(host, port);
                System.out.println("Socket created");
                socket.setSoTimeout(10000);

                NetworkAdapter networkAdapter = new NetworkAdapter(socket);
                this.gameState.setNetworkAdapter(networkAdapter);
                networkAdapter.setMessageListener(controller::messageReceived);

                this.gui.getNetworkUI().addToTextArea("Connection should be successful.\n");
                networkAdapter.writeNew(this.gameState.getUserName());
                networkAdapter.receiveMessagesAsync();
            }
            catch (IOException networkException) {
                this.gui.getNetworkUI().addToTextArea(" Connection Died. We are sorry! \n" + networkException.getMessage());
            }
        }
        catch (Exception controllerException) {
            this.gui.getNetworkUI().addToTextArea("Its really dead now\n" + controllerException.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HOST"))
        {
            hostClicked();
        }
        if (e.getActionCommand().equals("CONNECT"))
        {
            connectClicked();
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
