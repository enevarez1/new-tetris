/**
 *
 */
package model;

import java.util.*;


/** This class is the basis for a tetromino.
 * @author epadilla2
 *
 */
public abstract class Tetromino
        extends Object {
    //Iterable for collision.

    /**
     * 2d array for tetrominos
     */
    protected int[][] coordinates; //Field for coordinates of Tetrominos

    /**
     * @return Return coordinates.length, the Height of tetromino array
     */
    public int getHeight()
    {
        return this.coordinates.length;
    }

    /**
     * @return Returns coordinates[0].length, the Width of tetromino array
     */
    public int getWidth()
    {
        return this.coordinates[0].length;
    }

    //R3 in HW1


    //Collision Linked List

    /**
     * @return Returns collideList.iterator, a linked list to check for collision.
     */
    public Iterator<Collide<Integer, Integer>> collideIterator()
    {
        List<Collide<Integer, Integer>> collideList = new LinkedList<Collide<Integer, Integer>>();
        for (int i = 0; i < this.coordinates[0].length;i++)
        {
            for (int j = 0; j < this.coordinates.length;j++)
            {
                int value = this.coordinates[j][i];

                if (value == 1) collideList.add(new Collide((i), (j)));
            }
        }
        return collideList.iterator();
    }

    /**
     * Rotates Tetromino Right
     */

    public void rotateRight()
    {
        int[][] rotatedCoords = new int[this.coordinates[0].length][this.coordinates.length];
        //if tetromono is 1x4 it is now gonna occupy 4x1.

        for (int row = 0; row < this.coordinates.length;row++) //Using excel. Plot points and see what the transition is when you draw it out.
        {
            for (int col = 0; col < this.coordinates[0].length; col++) {
                rotatedCoords[col][this.coordinates.length - 1 - row] = this.coordinates[row][col];//Rotating rows if you are rotating right.
            }
        }
        this.coordinates = rotatedCoords; //Cords now take over the Temp array.
    }
    /**
     * Rotates tetromino to the left.
     */
    public void rotateLeft()
    {
        int[][] rotatedCoords = new int[this.coordinates[0].length][this.coordinates.length];
        //if tetrimono is 1x4 it is now gonna occupy 4x1.

        for (int row = 0; row < this.coordinates.length;row++) //Using excel. Plot points and see what the transition is when you draw it out.
        {
            for (int col = 0; col < this.coordinates[0].length; col++) {
                rotatedCoords[this.coordinates[0].length - 1 - col][row] = this.coordinates[row][col];//Rotating columns if you are rotating left.
            }
        }
        this.coordinates = rotatedCoords; //Cords now take over the Temp array.
    }

    /**
     * Enumeration to list the different tetrominos.
     * FILLER used when in multiplayer sending a line to the other player.
     * @author epadilla2
     *
     */
    protected TetrominoEnum tetroEnum;

    /**
     *
     * @return TetrominoEnum, the tetromino that will be used.
     */
    public TetrominoEnum getTetroType(){
        return this.tetroEnum; //Switch case in TetroMaker
    }

    public enum TetrominoEnum
    {
        /** Types of tetrominos, filler represents punishment lines added in multiplayer mode */

        I(0), J(1), L(2), O(3), S(4), Z(5), T(6), FILLER(7);
        //Constants for a switch case
        /** Integer value of each tetromino*/
        private int value;
        /**  Hash for inverse lookup of a tetromino based on value*/
        private static final Map<Integer, TetrominoEnum> reverseLookup = new HashMap<Integer, TetrominoEnum>();

        static {
            for (TetrominoEnum tetromino : TetrominoEnum.values()) {
                reverseLookup.put(tetromino.getValue(), tetromino);
            }
        }
        /**
         * Constructor that sets the integer value of tetromino
         * @param value Value Field
         */
        TetrominoEnum(int value)
        {
            this.value = value;
        }
        /**
         * @return Returns a random TetrominoEnum
         */
        public int getValue()
        {
            return value;
        }
        /**
         * @param value Value Field
         * @return Return TetrominoEnum depending on value
         */
        public static TetrominoEnum getEnumByValue(int value)
        {
            return reverseLookup.get(value);
        }
        /**
         * @return Returns a random TetrominoEnum
         */
        public static TetrominoEnum getRandomTetromino() {
            Random random = new Random();
            return values()[random.nextInt(values().length-1)];
        }
    }
}