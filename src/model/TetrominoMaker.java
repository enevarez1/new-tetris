package model;

import java.util.Iterator;

public class TetrominoMaker
{
    //Need to Make Tetrominos
    /*

    MAKE ALL BLOCK HORIZONTAL TO START

    I, L, J, T, O S and Z
    I is long 4 block
    L is 3 block vertical, one block on right side
    J is 3 block vertical, one block on left side
    T is 3 block horizontal, one block in the middle down
    O is 2x2 block
    S is 2 block horizontal, on 2nd block it goes up and 2 block horizontal to right
    Z is 2 block horizontal, on 1st block it goes up and 2 block horizontal to left
     */

    /**
     * @param tetrominoEnum Constant used for SwitchCase
     * @return Returns Tetromino based on tetrominoEnum
     */
    public Tetromino makeTetromino(Tetromino.TetrominoEnum tetrominoEnum)
    {
        Tetromino t = null; //Should never return null
        switch (tetrominoEnum)
        {
            case I:
                return new ITetromino();
            case L:
                return new LTetromino();
            case J:
                return new JTetromino();
            case O:
                return new OTetromino();
            case S:
                return new STetromino();
            case T:
                return new TTetromino();
            case Z:
                return new ZTetromino();

        }
        return t;
    }

    /**
     * @return Returns a random Tetromino for game
     */
    public Tetromino getRandomTetro()
    {
        return makeTetromino(Tetromino.TetrominoEnum.getRandomTetromino());
    }


    //Create class for each of these tetrominos
    //R2 in HW1

    /**
     * I shaped Tetromino
     */
    private class ITetromino extends Tetromino
    {
        //I is long 4 block horizontal. Using excel to draw
        ITetromino() {
            int[][] coordinates = {{1, 1, 1, 1}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.I; //Switch Case on Random Constants
        }
    }

    /**
     * L Shaped Tetromino
     */
    private class LTetromino extends Tetromino
    {
        //L is 3 block horizontal, one block on right side
        LTetromino() {
            int[][] coordinates = {{0,0,1},{1, 1, 1}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.L;//Switch Case on Random Constants
        }
    }

    /**
     * J Shaped Tetromino
     */
    private class JTetromino extends Tetromino
    {
        JTetromino() {
            int[][] coordinates = {{1,0,0}, {1, 1, 1}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.J; //Switch Case on Random Constants
        }
    }

    /**
     * T Shaped Tetromino
     */
    private class TTetromino extends Tetromino
    {
        TTetromino() {
            int[][] coordinates = {{0,1,0}, {1, 1, 1}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.T; //Switch Case on Random Constants
        }
    }

    /**
     * O Shaped Tetromino
     */
    private class OTetromino extends Tetromino
    {
        OTetromino() {
            int[][] coordinates = {{1, 1}, {1, 1}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.O; //Switch Case on Random Constants
        }
    }

    /**
     * S Shaped Tetromino
     */
    private class STetromino extends Tetromino
    {
        STetromino() {
            int[][] coordinates = {{0, 1, 1},{1, 1, 0}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.S; //Switch Case on Random Constants
        }
    }

    /**
     * Z Shaped Tetromino
     */
    private class ZTetromino extends Tetromino
    {
        ZTetromino() {
            int[][] coordinates = {{1, 1,0},{ 0, 1, 1}};
            this.coordinates = coordinates;
            this.tetroEnum = TetrominoEnum.Z; //Switch Case on Random Constants
        }
    }

}