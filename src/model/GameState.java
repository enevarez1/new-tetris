package model;

import java.util.Iterator;

/**
 * Model class from MVC. This class contains the Tretris logic but no GUI information.
 * Model must not know anything about the GUI and Controller.
 * @author epadilla2
 *
 */
public class GameState extends java.util.Observable{

    //Getting rid of board class.
    private Tetromino.TetrominoEnum[][] userBoard = new Tetromino.TetrominoEnum[20][10];
    private Tetromino.TetrominoEnum[][] newBoard;
    private TetrominoMaker tm;
    private int level = 69;
    private int score = 500;
    private int lines = 420;
    private boolean isGameActive;
    private boolean advanceTetro;
    private boolean hasGameEnded;
    private Tetromino tetromino;
    private Tetromino nextTetro = null;
    private int xPos;
    private int yPos;
    private boolean nine;
    private boolean fifteen;

    private boolean isMultiplayerGame;
    private int mpScore;
    private NetworkAdapter networkAdapter = null;
    private String userName;
    private String mpName;
    private boolean isMpGameOver;
    public int coordsUsed; //Coordinates that are value 1
    private Tetromino.TetrominoEnum[][] mpBoard;

    //LINES

    public NetworkAdapter getNetworkAdapter()
    {
        return this.networkAdapter;
    }

    public void setNetworkAdapter(NetworkAdapter networkAdapter)
    {
        this.networkAdapter = networkAdapter;
    }
    public String getUserName()
    {
        return this.userName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    public String getMpName()
    {
        return this.mpName;
    }
    public void setMpName(String mpName)
    {
        this.mpName = mpName;
    }
    public int getMpScore()
    {
        return this.mpScore;
    }
    public void setMpScore(int mpScore)
    {
        this.mpScore = mpScore;
    }

    public void newSingleGame()
    {
        this.isMultiplayerGame = false;
        newGame();
    }
    public void newMultiGame()
    {
        this.isMultiplayerGame = true;
        newGame();
    }
    public boolean isMultiplayerGame()
    {
        return this.isMultiplayerGame;
    }
    public boolean isGameActive()
    {
        return isGameActive;
    }

    /**
     * @return Return if game has ended
     */
    public boolean hasGameEnded() { return hasGameEnded; }
    /**
     *
     * @return Returns score
     */
    public int getScore()
    {
        return score;
    }
    /**
     * @return Return current level
     */
    public int getLevel()
    {
        return level;
    }

    /**
     * @return Return lines
     */
    public int getLines()
    {
        return lines;
    }

    public boolean isMpGameOver()
    {
        return this.isMpGameOver;
    }
    public void setMpGameOver(boolean isMpGameOver)
    {
        this.isMpGameOver = isMpGameOver;
    }
     /* Starts a new game of Tetris
     */
    public void newGame()
    {
        //if (getFifteen() == true) this.userBoard = new Tetromino.TetrominoEnum[15][15];
        //if (getNine() == true) this.userBoard = new Tetromino.TetrominoEnum[9][9];
        this.userBoard = new Tetromino.TetrominoEnum[20][10];        this.level=1;
        this.score=0;
        this.lines = 0;
        this.tm = new TetrominoMaker();
        this.nextTetro = tm.getRandomTetro();
        advanceNextTetro();
        this.isGameActive = true;
        this.hasGameEnded = false;
        System.out.println("Game shouldve started");
        //Board needs to be set
    }
    /**
     * Moves tetromino down
     */
    public void moveTetrominoDown()
    {
        //your code goes here
        this.yPos = this.yPos + 1;
        if(!validateTetrominoPosition()) this.yPos = this.yPos-1;
        validateTetrominoPosition();//Moving down is increasing on Applet
    }

    /**
     * Moves Tetromino Right
     */
    public void moveTetrominoRight()
    {
        //your code goes here
        this.xPos = this.xPos + 1;
        if (!validateTetrominoPosition()) this.xPos = this.xPos-1; //Reverse the call
    }

    /**
     * Moves Tetromino Left
     */
    public void moveTetrominoLeft()
    {
        //your code goes here
        this.xPos = this.xPos - 1;
        if (!validateTetrominoPosition()) this.xPos = this.xPos+1; //Reverse the call
    }

    /**
     * Rotates Tetromino Right
     */
    public void rotateTetroRight()
    {
        this.tetromino.rotateRight();
        if(!validateTetrominoPosition()) this.tetromino.rotateLeft();
    }

    /**
     * Rotates Tetromino Left
     */
    public void rotateTetroLeft()
    {
        this.tetromino.rotateLeft();
        if(!validateTetrominoPosition()) this.tetromino.rotateRight();
    }
    private boolean doesTetroHit(Collide<Integer, Integer> collide)
    {
        boolean check1 = (this.userBoard[this.yPos + ((Integer)collide.getY()).intValue()][this.xPos + ((Integer)collide.getX()).intValue()] != null); // Does it hit piece on board
        return check1;
    }

    /**
     * @param collide Point class used for the Array
     * @return Returns if Tetro causes game to end
     */
    public boolean doesTetroDie(Collide<Integer, Integer> collide)
    {
        boolean check1 = this.yPos + ((Integer)collide.getY()).intValue() + 1 <= (userBoard.length-1); //Does it hit above board?
        boolean check2 = this.userBoard[this.yPos + ((Integer)collide.getY()).intValue()][this.xPos + ((Integer)collide.getX()).intValue()] == null; //Does it next piece coming?

        return !(check1 && check2);

    }
    /**
     * @return Returns true if it is valid position
     */
    private boolean validateTetrominoPosition() {
        //your code goes here

        //Check if it within the board
        if (this.xPos < 0 || ((this.xPos + this.tetromino.getWidth() - 1) > (9))) return false;
        if (this.yPos < 0 || ((this.yPos + this.tetromino.getHeight() - 1) > (19))) return false;

        //Use Collide Iterator to check also

        Iterator<Collide<Integer, Integer>> checker = this.tetromino.collideIterator();

        while (checker.hasNext()) {
            Collide<Integer, Integer> collide = (Collide) checker.next();

            if (doesTetroHit(collide)) return false;
            if (doesTetroDie(collide)) {
                if (this.yPos <= 1) {
                    //Kill game
                    killGame();
                    return false;
                }
                stationaryTetro(); //Make Tetro STAY
                checkForLines(); //Check if they made a line
                System.out.println("Collider Next Tetro");
                advanceNextTetro(); //Next Tetro on the way
                if (this.isMultiplayerGame)
                {
                    sendStatus();
                }

            }
            setChanged();
            notifyObservers();
            return true;

            //Need to check for overlapping of pieces
        }
        if(this.isMultiplayerGame) sendStatus();
        return true;
    }

    /**
     * Advance Tetro Boolean
     */
    public void advanceTetro()
    {
        this.advanceTetro = true;
        moveTetrominoDown();
        this.advanceTetro = false;
    }

    /**
     * @return Return next Tetromino
     */
    public Tetromino getNextTetro(){
        return this.nextTetro;
    }

    /**
     * Spawn Next Tetro
     */
    public void advanceNextTetro()
    {
        this.tetromino = this.nextTetro;
        this.nextTetro = this.tm.getRandomTetro();
        this.xPos = 4;
        this.yPos = 0;
        this.coordsUsed += 4;
    }

    /**
     * Makes the tetromino stay still until needed to move because of line detection
     */
    public void stationaryTetro()
    {
        //Reusing code from board
        Tetromino.TetrominoEnum type = this.tetromino.getTetroType();
        Iterator<Collide<Integer, Integer>> checker = this.tetromino.collideIterator();
        while (checker.hasNext()) {
            Collide<Integer, Integer> collide = (Collide) checker.next();
            this.userBoard[this.yPos + ((Integer)collide.getY()).intValue()][this.xPos + ((Integer)collide.getX()).intValue()] = type;
        }
        this.score += 5; //5pts for adding a tetro
        System.out.println("Stationary Tetro");
    }

    //Need to show board

    /**
     * @return Returns a copy of the Users Board
     */
    public Tetromino.TetrominoEnum[][] getUserBoard() {

        //copy array and the board
        Tetromino.TetrominoEnum[][] copy = new Tetromino.TetrominoEnum[this.userBoard.length][];
        for (int i = 0; i < this.userBoard.length;i++)
        {
            copy[i] = (Tetromino.TetrominoEnum[])this.userBoard[i].clone();
        }

        //This is used to keep track of pieces on board
        Tetromino.TetrominoEnum type = this.tetromino.getTetroType();
        Iterator<Collide<Integer, Integer>> iterator = this.tetromino.collideIterator();
        while (iterator.hasNext())
        {
            Collide<Integer, Integer> collide = (Collide)iterator.next();
            copy [this.yPos + ((Integer)collide.getY()).intValue()][this.xPos + ((Integer)collide.getX()).intValue()] = type;

        //Tetromino.TetrominoEnum type = Tetromino.TetrominoEnum.I;

        //this.tetromino = new Tetromino() {
         //   @Override
         //   public Iterator<Collide<Integer, Integer>> iterator() {
         //       Collide collide = (Collide) iterator();
          //      copy[yPos + ((Integer) collide.getY())][xPos + ((Integer) collide.getY())] = type;
//
          //      return (Iterator<Collide<Integer, Integer>>) collide;
//
          //  }
//
        }

//        for (Collide<Integer, Integer> aTetromino : this.tetromino) {
//            Collide collide = (Collide) aTetromino;
//            copy[this.yPos + ((Integer) collide.getY())][this.xPos + ((Integer) collide.getY())] = type;
//        }

        return copy;
    }

    /**
     * Check for lines at bottom of board
     */
    public void checkForLines()
    {
        int methodLines = 0;
        boolean isLine = true;
        for(int i = 0; i < this.userBoard.length; i++)
        {
            for (int j = 0; j < this.userBoard[0].length; j++)
            {
                if (this.userBoard[i][j] == null)
                {
                    isLine = false;
                    break;
                }
            }
            methodLines++;
            if (isLine)
            {
                removeLine(methodLines);
                //Divide by 10 for level
                this.lines++;
                if (this.lines % 10 == 0) this.level++;
            }

        }
        switch (methodLines)
        {
            case 1:
                this.score += 100;
                break;
            case 2:
                this.score += 300;
                if(this.isMultiplayerGame)
                    this.networkAdapter.writeFill(1);
                break;
            case 3:
                this.score += 600;
                if(this.isMultiplayerGame)
                    this.networkAdapter.writeFill(2);
                break;
            case 4:
                this.score += 1000;
                if(this.isMultiplayerGame)
                    this.networkAdapter.writeFill(4);
                break;
        }
    }

    /**
     * Remove lines based on how many lines
     * @param line lines in the game that need to be cleared
     */
    public void removeLine(int line)
    {
        for (int i = line; i > 0; i--)
        {
            this.userBoard[i] = this.userBoard[i - 1];
        }

        this.userBoard[0] = new Tetromino.TetrominoEnum[10]; //Remove at bottom of board
    }

    public void getStatus(Boolean x) {
        isGameActive = x;
    }

    public void setGameActive(Boolean signal){
        isGameActive = signal;
    }

    /**
     * Game Over for user
     */
    private void killGame()
    {
        this.isGameActive = false;
        this.hasGameEnded = true;
        if (this.isMultiplayerGame)
        {
            if(this.isMpGameOver)
            {
                this.networkAdapter.writeQuit();
                this.networkAdapter.close();
            }
        }
    }

    /**
     * Sends status for the socket
     */
    public void sendStatus()
    {
        System.out.println("SendBoard called");
        Tetromino.TetrominoEnum[][] mpBoard = getUserBoard();


        //use 3 coordinates X, Y, value of tetro according to professor

        int[] boardCoords = new int[this.coordsUsed * 3];//Coordinates that are occupied, * 3 for professor
        /*int counter = 0;

        for (int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if(mpBoard[j][i] != null)
                {
                    //Multiply by 3
                    boardCoords[counter * 3] = j;
                    boardCoords[counter * 3 + 1] = i;
                    boardCoords[counter * 3 + 2] = mpBoard[j][i].getValue(); //Value of Tetro as discussed by Prof

                    counter++;
                }
            }
        }*/
        this.networkAdapter.writeStatus(this.score, this.isGameActive ? 1 : 0, boardCoords ); //1 for true, 0 for false.
    }

    /**
     * Setting the multiplayerBoard
     * @param mpBoard TetrominoEnum board
     */
    public void setMPBoard(Tetromino.TetrominoEnum[][] mpBoard)
    {
        this.mpBoard = mpBoard;
    }

    /**
     * Get the MP Board
     * @return TetrominoEnum[][] mpBoard
     */
    public Tetromino.TetrominoEnum[][] getMPBoard()
    {
        return this.mpBoard;
    }
}
