package model;

public class Collide<X, Y> extends Object{

    //Using this to track when a tetromino collides

    private final X xVal;
    private final Y yVal;

    /**
     * Sets X and Y values for collision
     * @param xVal xValue of Board
     * @param yVal yValue of Board
     */
    public Collide(X xVal, Y yVal)
    {
        this.xVal = xVal;
        this.yVal = yVal;
    }

    /**
     * Returns X value
     * @return Return xVal of Board
     */
    public X getX() {
        return (X)xVal;
    }

    /**
     * Returns Y value
     * @return Returns yVal of Board
     */
    public Y getY() {
        return (Y)yVal;
    }
}
