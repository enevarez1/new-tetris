package view;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
//import javafx.embed.swing.JFXPanel;
import model.Collide;
import model.GameState;
import model.Tetromino.TetrominoEnum;
import view.animation.AnimationApplet;
import model.Tetromino;
import java.awt.Font;

import javax.sound.sampled.*;
import javax.swing.ImageIcon;
//import javafx.scene.media.Media;
// javafx.scene.media.MediaPlayer;

import javax.swing.*;
import javax.swing.JDialog;


public class ChatUI extends JDialog
{
    static Socket sckt;
    static DataInputStream dtinpt;
    static DataOutputStream dtotpt;
    /** Creates new form client */
    public ChatUI() {
        initComponents();
    }

    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {


    }// </editor-fold>
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
    {
        // TODO add your handling code here:


        try
        {
            String msgout = "";
            msgout = jTextField1.getText().trim();
            dtotpt.writeUTF(msgout);
        }
        catch (Exception e)
        {
        }
    }

    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new ChatUI().setVisible(true);
            }
        });

        try
        {
            sckt = new Socket("localhost",6969);
            dtinpt = new DataInputStream(sckt.getInputStream());
            dtotpt = new DataOutputStream(sckt.getOutputStream());
            String msgin = "";
            while(!msgin.equals("Exit"))
            {
                msgin=dtinpt.readUTF();
                jTextArea1.setText(jTextArea1.getText().trim()+"\n Server:"+msgin);
            }
        }
        catch(Exception e)
        {

        }
    }

    // Variables declaration - do not modify
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private static javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration

}

