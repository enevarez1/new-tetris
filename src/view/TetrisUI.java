package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
//import javafx.embed.swing.JFXPanel;
import controller.Controller;
import model.Collide;
import model.GameState;
import model.Tetromino.TetrominoEnum;
import view.animation.AnimationApplet;
import model.Tetromino;
import java.awt.Font;

import javax.sound.sampled.*;
import javax.swing.ImageIcon;
//import javafx.scene.media.Media;
// javafx.scene.media.MediaPlayer;

import javax.swing.*;

/**
 * Creates main user interface. See AnimationApplet to see what is being inherited.
 * @author epadilla2
 *
 */
@SuppressWarnings("serial")
public class TetrisUI extends AnimationApplet implements Observer {
//    final JFXPanel fxPanel = new JFXPanel();
//    public static final String SONG = "src/media/tetris-song.mp3";
//    Media MP3Player = new Media(new File(SONG).toURI().toString());
//    MediaPlayer mediaPlayer = new MediaPlayer(MP3Player);
    /**
     *
     */

    AudioInputStream audioInputStream;

    {
        try {
            audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource("/media/wavtetris-song.wav"));
            Clip clip;

            {
                try {
                    clip = AudioSystem.getClip();
                    clip.open(audioInputStream);
                    clip.start();
                } catch (LineUnavailableException e) {
                    e.printStackTrace();
                }
            }
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private GameState gameState;
    private boolean gamePaused = true;
    private NetworkUI networkUI;
    private Controller controller;
    private ChatUI ChatUI;

    private KeyListener keyListener = null;


    public TetrisUI(String[] args) {
        super(args);
        this.networkUI = new NetworkUI();
        this.ChatUI = new ChatUI();
    }

    public NetworkUI getNetworkUI() { return this.networkUI;}

    public ChatUI getChatUI() { return this.ChatUI;}


    /**
     * init on AnimationApplet
     */
    public void init() {
        this.delay = 500;
        super.init();
    }
    public void resetTimer() { this.timer = new Timer(this.delay, e -> periodicTask());}

    /**
     * start on AnimationApplet
     */
    public void start() {
        super.start();
        repaint();
        this.gamePaused = false;
    }

    /**
     * Stop on AnimationApplet
     */
    public void stop() {
        super.stop();
        this.gamePaused = true;
    }

    /**
     * This will be called based on the timer
     */
    public void periodicTask() {
        if (this.gameState.isGameActive() || this.gameState.hasGameEnded()) this.gameState.advanceTetro();
        super.periodicTask();
    }

    /**
     * Here goes what is going to drawn on screen
     */
    protected void paintFrame(Graphics g) {
        if (g == null) return;
        makeGame(g);

    }

    public boolean isGamePaused() {
        return this.gamePaused;
    }

    public void setGame(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Drawing the board and tetros with timer
     *
     * @param g Graphics from applet
     */
    public void makeGame(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.dim.width, this.dim.height);
        Color pretty = new Color(173, 216, 230);
        g.setColor(pretty);
        g.fillRect(25, 25, 250, 500);

        if (this.gameState.isGameActive() || this.gameState.hasGameEnded()) {
            drawTetros(g, this.gameState.getUserBoard(), 25, 25, 25);
        }
        drawBoard(g, 25, 25, 25);
        drawStatusMenu(g);
    }

    /**
     * Draws Tetros based one what is filled on board.
     *
     * @param g         Graphics from applet
     * @param userBoard Board that is used by user
     * @param area      Area of block
     * @param row       Row of pixel
     * @param col       Column of Pixel
     */
    public void drawTetros(Graphics g, TetrominoEnum[][] userBoard, int area, int row, int col) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 20; j++) {
                if (userBoard[j][i] != null) //Start from top of board
                {
                    g.setColor(getTetrominoColor(userBoard[j][i]));
                    g.fillRect((row + i * area), (col + j * area), area, area);
                }
            }
        }

    }

    /**
     * Draws the lines of the board to make a grid
     *
     * @param g    Graphics from applet
     * @param area Area of block
     * @param row  Row of pixel
     * @param col  Column of pixel
     */
    public void drawBoard(Graphics g, int area, int row, int col) {
        g.setColor(Color.BLACK);
        for (int i = 0; i <= 10; i++) {
            g.drawLine(row + i * area, col, row + i * area, col + area * 20);
            //row + i * area,col, row+ i * area, col+area*15
        }
        for (int i = 0; i <= 20; i++) {
            g.drawLine(row, col + i * area, row + area * 10, col + i * area);
        }
    }

    /**
     * @param tetrominoEnum get the Tetromino
     * @return a color based on the tetromino type
     */
    private Color getTetrominoColor(Tetromino.TetrominoEnum tetrominoEnum) {
        Color color = null;
        switch (tetrominoEnum) {
            case I:
                color = Color.RED;
                break;
            case J:
                color = Color.GREEN;
                break;
            case L:
                color = Color.PINK;
                break;
            case O:
                color = Color.CYAN;
                break;
            case S:
                color = Color.MAGENTA;
                break;
            case Z:
                color = Color.YELLOW;
                break;
            case T:
                color = Color.ORANGE;
                break;
            default:
                color = Color.WHITE;
                break;
        }//end switch
        return color;
    }//end getTetrominoColor

    /**
     * When there is a change on the model, the View (GUI) gets notified (this method is called)
     */
    public void update(Observable obs, Object obj) {
        repaint();

    }

    /**
     * @return Returns Key Listener
     */
    public KeyListener getKeyListener() {
        return this.keyListener;
    }

    /**
     * Sets Keylistener
     *
     * @param keyListener Listen to key functions
     */
    public void setKeyListener(KeyListener keyListener) {
        this.keyListener = keyListener;
    }

    /**
     * @return Top menu bar and its buttons, it overrides methods from AnimationApplet
     */
    protected JMenuBar createMenu() {
        JMenuBar menuBar = new JMenuBar();
//        menuBar.setBackground(Color.WHITE);//White Background

        JMenu optionsMenu = new JMenu("Game");
//        optionsMenu.setBackground(Color.WHITE);
//        optionsMenu.setForeground(Color.BLACK);
//
//        menuBar.add(optionsMenu);

        return menuBar;
    }

    @Override
    protected JPanel createUI() {
        JMenu dropdownMenu = new JMenu("Menu");
        JMenuBar mainMenu = createMenu();
        JPanel control = new JPanel();
        control.setLayout(new FlowLayout());
        JLabel connectionColor;
//        JLabel messageBottom
        this.showStatus("Welcome to Tetris");

        //status bar
        connectionColor = new JLabel();
        connectionColor.setOpaque(true);
        connectionColor.setBackground(Color.RED);

        //icons
        ImageIcon controls = new ImageIcon(getClass().getResource("/images/gamepad.png"));
        ImageIcon quitter = new ImageIcon(getClass().getResource("/images/quit.png"));
        ImageIcon singleIcon = new ImageIcon(getClass().getResource("/images/console.png"));
        ImageIcon multiIcon = new ImageIcon(getClass().getResource("/images/online-game.png"));
        ImageIcon pauseIcon = new ImageIcon(getClass().getResource("/images/pause.png"));
        ImageIcon chat = new ImageIcon(getClass().getResource("/images/speech-bubble.png"));


        //JMenu Items
        JMenu newGame = new JMenu("New Game");
        JMenuItem pauseGame = new JMenuItem("Pause", pauseIcon);
        JMenuItem controlKeys = new JMenuItem("Control Keys", controls);
        JMenuItem chatBtn = new JMenuItem("Chat", chat);
        JMenuItem quitBtn = new JMenuItem("Quit", quitter);

        //sub menus
        //JMenuItem fifteenGrid = new JMenuItem("15 x 15");
        //JMenuItem nineGrid = new JMenuItem("9 x 9");
        JMenuItem singlePlayer = new JMenuItem("Single Player", singleIcon);
        JMenuItem multiPlayer = new JMenuItem("Multiplayer", multiIcon);

        dropdownMenu.add(newGame);
        //newGame.add(fifteenGrid);
        //newGame.add(nineGrid);
        newGame.add(singlePlayer);
        newGame.add(multiPlayer);
        dropdownMenu.add(pauseGame);
        dropdownMenu.add(controlKeys);
        dropdownMenu.addSeparator();
        dropdownMenu.add(chatBtn);
        dropdownMenu.addSeparator();
        dropdownMenu.add(quitBtn);

        mainMenu.add(dropdownMenu);


        //mnemomics and accelerators
        dropdownMenu.setMnemonic('m');
        pauseGame.setMnemonic('p');
        pauseGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.CTRL_MASK));
        pauseGame.setAccelerator(KeyStroke.getKeyStroke((char) KeyEvent.VK_ESCAPE));
        quitBtn.setMnemonic('q');
        quitBtn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Event.CTRL_MASK));
        chatBtn.setMnemonic('h');
        chatBtn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, Event.CTRL_MASK));
        controlKeys.setMnemonic('c');
        controlKeys.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK));
        newGame.setMnemonic('n');
        //fifteenGrid.setMnemonic('5');
        //nineGrid.setMnemonic('9');
        singlePlayer.setMnemonic('s');
        multiPlayer.setMnemonic('m');


        /**
         * Menu button listeners
         */



        /*fifteenGrid.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = JOptionPane.showConfirmDialog(null, "Do you want to quit?");
                gameState.newGame();
                System.out.println("15x15 starting");
            }
        });

        nineGrid.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = JOptionPane.showConfirmDialog(null, "Do you want to quit?");
                gameState.newGame();
                System.out.println("9x9 starting");
            }
        });*/

        singlePlayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(gameState.isGameActive()) {

                    int input = JOptionPane.showConfirmDialog(null, "Do you want to quit?");
                    // 0=yes, 1=no, 2=cancel
                    if (input == 1){
                        gameState.newGame();
                    }

                }
                gameState.newGame();
                System.out.println("SinglePlayer starting");
            }
        });

        multiPlayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(gameState.isGameActive()) {

                    /*int input = JOptionPane.showConfirmDialog(null, "Do you want to quit?");
                    if (input == 1) {
                        gameState.newMultiGame();
                    }*/
                }
                multiplayerClicked();
                System.out.println("Multiplayer starting");
            }
        });
        controlKeys.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("controls menu show");
                JOptionPane.showMessageDialog(controlKeys
                        , "Right Arrow Key: Move tetromino to the right.\n"
                                + "Left Arrow Key: Move tetromino to the left.\n"
                                + "Down Arrow Key: Push tetromino down.\n"
                                + " \"A\" Key: Rotate tetromino right.\n"
                                + " \"Z\" Key: Rotate tetromino left.\n"
                                + "Escape Key: Pause-Start game."
                        , "Controls",
                        JOptionPane.PLAIN_MESSAGE
                );
            }
        });

        chatBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("chat dialog should open");
                this.getChatUI().setVisible(true);


            }
        });


        quitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Game should quit");
                System.exit(1);
            }
        });

        pauseGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Game should pause");
                if(gameState.isGameActive()) {
                    gameState.setGameActive(false);
                    TetrisUI.super.stop();

                }
                else{
                    gameState.setGameActive(true);
                    TetrisUI.super.start();

                }
            }
        });


        JPanel root = new JPanel();
        JPanel secondRoot = new JPanel();
        root.setLayout(new BorderLayout());
        root.add(mainMenu, BorderLayout.NORTH);
        root.add(this, BorderLayout.CENTER);
        root.add(statusBar, BorderLayout.SOUTH);
        secondRoot.setBorder(BorderFactory.createTitledBorder("this should be red"));
        secondRoot.add(connectionColor,BorderLayout.SOUTH);


//        control.add(mainMenu);
        return root;
    } //end jpanel

    private void multiplayerClicked() {
        String name = this.gameState.getUserName();
        name = (name == null) ? "" : name;
        while (name.equals(""))
            name = JOptionPane.showInputDialog(null, "Please enter your name", "Player Name", 1);
        this.gameState.setUserName(name);
        this.setGame(this.gameState);
        this.getNetworkUI().setVisible(true);
    }

    /**
     * Draws the status menu next to the game board
     *
     * @param g Graphics Object for Applet
     */
    protected void drawStatusMenu(Graphics g) {
        g.setColor(Color.BLACK);
        for (int i = 0; i < 10; i++) {
            g.drawRect(345 + i, 25 + i, 130, 135);
        }
        g.fillRect(345, 70, 130, 10);
        g.fillRect(345, 120, 130, 10);

        g.drawString("Level: " + this.gameState.getLevel(), 360, 55);
        g.drawString("Lines: " + this.gameState.getLines(), 360, 105);
        g.drawString("Score: " + this.gameState.getScore(), 360, 155);


        for (int i = 0; i < 5; i++) {
            g.drawRect(345 + i, 200 + i, 180, 125);
        }
        Font myFont = new Font ("Courier New", 1, 17);
        g.setFont(myFont);
        g.drawString("Next Tetromino", 365,225);
        if (this.gameState.isGameActive()) {
            Tetromino nextTetro = this.gameState.getNextTetro();

            //Iterate over next Tetro, not included on Board so copy the getUserBoard() code
            Iterator<Collide<Integer, Integer>> checker = nextTetro.collideIterator();
            Color color = getTetrominoColor(nextTetro.getTetroType());
            while (checker.hasNext()) {

                Collide<Integer, Integer> pair = (Collide) checker.next();

                g.setColor(color);
                g.fillRect(385 + ((Integer) pair.getX()).intValue() * 25, 250 + ((Integer) pair.getY()).intValue() * 25, 25, 25);
                g.setColor(Color.BLACK);
                g.drawRect(385 + ((Integer) pair.getX()).intValue() * 25, 250 + ((Integer) pair.getY()).intValue() * 25, 25, 25);
            }

//        if(gameState.getScore() == 500 ){
//            mediaPlayer.play();
//
        }
        //Draw Oppenent Board
        if (this.gameState.isMultiplayerGame())
        {
            TetrominoEnum[][] mpBoard = gameState.getMPBoard();
            g.setColor(Color.BLACK);
            for (int i = 0; i < 5; i++) {
                g.drawRect(345 + i, 325 + i, 180, 320);
            }

            g.setColor(Color.BLACK);
            g.drawString(this.gameState.getMpName(), 360, 355);

            g.drawString("Score: "+this.gameState.getMpScore(), 360, 375);


            g.setColor(Color.cyan);
            g.fillRect(385, 400, 110, 220);

            if (mpBoard != null)
                drawTetros(g, mpBoard, 11, 385, 400);
            drawBoard(g, 11, 385, 400);

            if (this.gameState.getMpScore() <= this.gameState.getMpScore()){
                this.showStatus("Losing");
            }
            else{
                this.showStatus("Winning");
            }
        }
    }

    public void setActionListener(Controller controller) {
        this.controller = controller;
    }
}//end TetrisUI class
