package view;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
//import javafx.embed.swing.JFXPanel;
import model.Collide;
import model.GameState;
import model.Tetromino.TetrominoEnum;
import view.animation.AnimationApplet;
import model.Tetromino;
import java.awt.Font;

import javax.sound.sampled.*;
import javax.swing.ImageIcon;
//import javafx.scene.media.Media;
// javafx.scene.media.MediaPlayer;

import javax.swing.*;
import javax.swing.JDialog;


public class NetworkUI extends JDialog
{
    private final Dimension DIMENSION = new Dimension(500,300);
    public JButton connectButton;
    public JButton hostButton;
    public JTextField hostText;
    public JTextField portText;

    private JTextArea textArea;
    private JPanel networkUIPanel;
    private ActionListener actionListener;
    public NetworkUI()
    {

    }

    public void run()
    {
        this.makeNetworkUI();
        setSize(this.DIMENSION);

    }
    public void addToTextArea(String text)
    {
        this.textArea.append(text);
        this.textArea.repaint();
    }

    public void makeNetworkUI()
    {
        this.networkUIPanel = new JPanel(new FlowLayout(5));
        this.connectButton = new JButton("Connect");
        this.hostButton = new JButton("Host");
        this.connectButton.setFocusPainted(false);
        this.hostButton.setFocusPainted(false);
        this.hostText = new JTextField("localhost",10);
        this.portText = new JTextField("6969",10);
        this.networkUIPanel.add(this.connectButton);
        this.networkUIPanel.add(this.hostButton);
        this.networkUIPanel.add(this.hostText);
        this.networkUIPanel.add(this.portText);
        this.textArea = new JTextArea(10,10);
        this.textArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(this.textArea);
        this.setLayout(new BorderLayout());
        this.add(this.networkUIPanel, "North");
        this.add(textArea, "South");

    }
    public void setActionListener (ActionListener actionListener)
    {
        this.hostButton.addActionListener(actionListener);
        this.hostButton.setActionCommand("HOST");
        this.connectButton.addActionListener(actionListener);
        this.connectButton.setActionCommand("CONNECT");
    }
}
